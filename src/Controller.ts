


import { Console } from 'console';
import fsP from 'fs/promises';
import {SimpleObject,Task} from './GlobalTypes.js';
import * as Repository from './DataLayer.js'
import * as View from './View.js'

enum MenuOptions {
    Create = "create",
    ShowAllTasks = "read",
    Update = "update",
    Delete = "delete",
    DeleteCompletedTasks = "delcompleted",
    Help = "help"
  }

enum ReadOptions{
    ALL = "ALL",
    ONLY_ACTIVE = "ACTIVE",
    ONLY_COMPLETED = "COMPLETED"
}

function createTask(stateObj :SimpleObject, taskMessage: string) {
    const taskObj =
    {
        taskID: stateObj.nextID,
        taskMessage,
        isChecked: false
    }
    stateObj.nextID++;
    stateObj.tasksArr.push(taskObj)
}

function showAllTasks(stateObj: SimpleObject,filterOption : string) {
    let tasksToRender = stateObj.tasksArr;
    if(filterOption !== ReadOptions.ALL){
        const isCheckedFilter =  filterOption === ReadOptions.ONLY_ACTIVE? false : true;
         tasksToRender = stateObj.tasksArr.filter((task : Task) => task.isChecked === isCheckedFilter)    
       }
    View.renderAllTasks(tasksToRender)
}

function deleteTask(stateObj : SimpleObject, taskID: number) {
    stateObj.tasksArr = stateObj.tasksArr.filter((task:Task) => (task.taskID !== taskID))
}

function updateTask(stateObj : SimpleObject, taskID : number, taskMessage: string, toggleStatus: boolean) {

    stateObj.tasksArr.forEach((task : Task) => {
        if (task.taskID === taskID) {
            task.taskMessage = taskMessage
            if (toggleStatus) {
                task.isChecked = !(task.isChecked);
            }
        }
    });
}

function deleteCompletedTasks(stateObj: SimpleObject) : void{

    stateObj.tasksArr =  stateObj.tasksArr.filter((task : Task) =>{
        return (task.isChecked === false);
    })

}

export async function start() {
    const source_file = `./todos.json`
    const stateObj: SimpleObject = {}

    await Repository.initModel(stateObj, source_file);
    
    //gets user request
    let answer = (process.argv[2]).toLowerCase()
    const id = Number (process.argv[3])
    switch (answer) {
        case MenuOptions.Create:
            let taskMessage = process.argv[3]
            createTask(stateObj, taskMessage)
            break;

        case MenuOptions.ShowAllTasks:
            const readOption = process.argv[3] ?process.argv[3].toUpperCase() : ReadOptions.ALL
            showAllTasks(stateObj,readOption)
            break;

        case MenuOptions.Update:
            const taskContent = process.argv[4];
            let toggleStatus = process.argv[5];
            let toggleStatusBool = toggleStatus === 'y' ? true : false;
            updateTask(stateObj, id, taskContent, toggleStatusBool)
            break;

        case MenuOptions.Delete:
            deleteTask(stateObj, id)
            break;

        case MenuOptions.DeleteCompletedTasks:
            deleteCompletedTasks(stateObj)
            break;

        case MenuOptions.Help:
           View.showGuide();
            break;

        default:
         View.sendNotValidMessage();
    }
    
    await Repository.writeToDB(stateObj, source_file)
    console.log("bye bye")
}