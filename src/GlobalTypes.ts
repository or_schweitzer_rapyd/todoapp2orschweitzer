export interface SimpleObject {
    [key: string]: any ;
  }

export interface Task {
    taskID : number,
    taskMessage: string,
    isChecked: Boolean
}