import fsP from 'fs/promises';
import {SimpleObject} from './GlobalTypes.js'

export async function initModel(stateObj:SimpleObject, source_file:string) {
    try {
        const rawData = await fsP.readFile(source_file, 'utf8')
        const jsonData = JSON.parse(rawData);
        stateObj.tasksArr = jsonData.tasksArr;
        stateObj.nextID = jsonData.nextID;
    } catch (e) {
        stateObj.tasksArr = []
        stateObj.nextID = 0;
    }

}

export async function writeToDB(stateObj:SimpleObject, source_file:string) {

    await fsP.writeFile(source_file, JSON.stringify(stateObj, null, 2))
}