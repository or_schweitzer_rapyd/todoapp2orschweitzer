import {SimpleObject,Task} from './GlobalTypes.js';


export function renderAllTasks(tasks : Task [] ) {
    tasks.forEach((task : Task) => {
        console.log(`${task.taskID}. ${task.taskMessage} ${task.isChecked?`Completed` : `Active`}`)
    })
}
export function showGuide(){
    console.log(`Please run one of the following options:
    node ./dist/index.js create YOUR_TASK_CONTENT
    node ./dist/index.js read [ALL | ACTIVE| COMPLETED]
    node ./dist/index.js update TASK_ID TASK_CONTENT CHANGE_STATUS[y|n]
    node ./dist/index.js delete TASK_ID
    node ./dist/index.js delCompleted,
    `)
}

export function sendNotValidMessage(){
    console.log('Sorry ,this is not a valid option')
}